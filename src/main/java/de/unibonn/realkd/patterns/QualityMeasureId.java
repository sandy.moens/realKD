/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import de.unibonn.realkd.common.measures.Measure;

/**
 * Enumeration of measurement identifiers for which measurements can be bound to
 * patterns.
 * 
 * @see Pattern
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public enum QualityMeasureId implements Measure {

	LIFT("lift", "Normalized difference between actual frequency and expected frequency of pattern."), 
	NEGATIVE_LIFT("negative lift", "Positive lift or zero in case actual lift is negative."), 
	ABSOLUTE_LIFT("absolute lift", "Absolute lift value."), 
	PRODUCT_OF_INDIVIDUAL_FREQUENCIES("expected frequency", "Expected frequency when assuming independenc of single propositions."), 
	DEVIATION_OF_FREQUENCY("deviation of frequency", "Difference between actual frequency and expected frequency of pattern."), 
	OUTLIER_SCORE("outlier score", ""),
	GLOBAL_STD("global std. dev.", "Squareroot of 1-norm of attribute sample covariance matrix in global population."),
	LOCAL_STD("local std. dev.", "Squareroot of 1-norm of attribute sample covariance matrix in subgroup."),
	AREA("area", "The true size of the pattern in the data as a combination of the support and the size of the pattern."), 
	SUPPORT("support", "Absolute occurance frequency of the pattern in the complete data."), 
	CONFIDENCE("confidence", "The conditional probability of the consequent occurring given that the antecedent occurs."), 
	WEIBULL_SCALE("scale","The scale parameter of the fitted distribution."),
	GLOBAL_WEIBULL_SCALE("global scale","The scale parameter of the Weibull distribution fitted to the global population"),
	LOCAL_WEIBULL_SCALE("local scale","The scale parameter of the Weibull distribution fitted to the local population"), 
	LOCAL_RMSE("local rmse","The root mean squared error of the local model"),
	REFERENCE_RMSE("ref. rmse","The root mean squared error of the reference model."),
	REF_ENTROPY("ref. entropy","Shannon entropy of reference model."),
	LOCAL_ENTROPY("local entropy","Shannon entropy of local model."),
	LOCAL_MODE_PROBABILITY("local mode probability", "Maximum probability cell of local model."),
	GLOBAL_PEARSON("ref. Pearson correlation",""),
	LOCAL_PEARSON("local Pearson correlation",""),
	COHESION("cohesion","Indicates the cohesion or the tightness of a sequence pattern."),
	INTERESTINGNESS("interestingness","The harmonic mean between the frequency and the cohesion of a pattern.")
	;

	private final String name;
	
	private final String description;

	private QualityMeasureId(String name, String description) {
		this.name = name;
		this.description = description;
	}

	@Override
	public String caption() {
		return name;
	}

	@Override
	public String description() {
		return description;
	}

}
