/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.pmm;

import static de.unibonn.realkd.patterns.Frequency.FREQUENCY;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternBuilder;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.subgroups.Subgroup;

/**
 * <p>
 * Provides static factory methods for the construction of simple model mining
 * subgroups as well as measurement procedures for pure model mining.
 * </p>
 * <p>
 * Pure model subgroups capture a subset of a population for which a selection
 * of target attributes can be represented "purely" by a certain model class.
 * The meaning of "purely" can vary between low prediction or fitting error, or
 * simply low target attribute variance.
 * </p>
 * <p>
 * In contrast to exceptional model mining, pure model mining is agnostic to the
 * global data distribution. However, depending on the specific configuration,
 * pure model mining is highly non-agnostic to the local data distribution,
 * which it explicitly tests for good fits of distributions that may have a
 * strong bias.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 * 
 * @see ExceptionalModelMining
 *
 */
public class PureModelMining {

	/**
	 * Creates a new pure model subgroup pattern based on a provided descriptor,
	 * a purity measurement procedure, and a list of optional measurement
	 * procedures. In addition to the specified procedures the resulting
	 * subgroup pattern will also contain a frequency measurement.
	 * 
	 * @param descriptor
	 *            the descriptor of the new subgroup
	 * @param purityMeasurementProcedure
	 *            the procedure to be used to measure model purity of the
	 *            subgroup
	 * @param optionalMeasurementProcedures
	 *            more optional procedures to be applied for the new subgroup
	 * @return new pure model subgroup pattern
	 */
	public static PureModelSubgroup pureSubgroup(final Subgroup<?> descriptor,
			final MeasurementProcedure<? extends Measure, ? super Subgroup<?>> purityMeasurementProcedure,
			final List<MeasurementProcedure<? extends Measure, ? super Subgroup<?>>> optionalMeasurementProcedures) {

		List<MeasurementProcedure<? extends Measure, ? super Subgroup<?>>> list = Lists.newArrayList();
		list.add(FREQUENCY);
		list.add(purityMeasurementProcedure);
		list.addAll(optionalMeasurementProcedures);

		List<Measurement> measurements = list.stream().map(p -> p.perform(descriptor)).collect(Collectors.toList());

		List<Measurement> allMeasurements = new ArrayList<>(measurements);
		// allMeasurements.addAll(Subgroups.descriptiveModelMeasurements(descriptor));

		List<MeasurementProcedure<? extends Measure, ? super Subgroup<?>>> optionalProceduresSnapshot = ImmutableList
				.copyOf(optionalMeasurementProcedures);

		return new PureModelSubgroupImplementation(descriptor.population(), descriptor, allMeasurements,
				purityMeasurementProcedure.getMeasure(),
				pattern -> new PureModelSubgroupBuilderImplementation(pattern.descriptor().serialForm(),
						purityMeasurementProcedure, optionalProceduresSnapshot));
	}

	private PureModelMining() {
		;
	}

	private static class PureModelSubgroupBuilderImplementation
			implements PatternBuilder<Subgroup<?>, PureModelSubgroup> {

		private SerialForm<? extends Subgroup<?>> descriptorBuilder;

		private MeasurementProcedure<? extends Measure, ? super Subgroup<?>> purityMeasurementProcedure;

		private ArrayList<MeasurementProcedure<? extends Measure, ? super Subgroup<?>>> additionalMeasurementProcedures = new ArrayList<>();

		@JsonCreator
		private PureModelSubgroupBuilderImplementation(
				@JsonProperty("descriptorBuilder") SerialForm<? extends Subgroup<?>> descriptorBuilder,
				@JsonProperty("purityMeasurementProcedure") MeasurementProcedure<? extends Measure, ? super Subgroup<?>> purityMeasurementProcedure,
				@JsonProperty("additionalMeasurementProcedures") List<MeasurementProcedure<? extends Measure, ? super Subgroup<?>>> additionalMeasurementProcedures) {
			this.descriptorBuilder = descriptorBuilder;
			this.purityMeasurementProcedure = purityMeasurementProcedure;
			this.additionalMeasurementProcedures = Lists.newArrayList(additionalMeasurementProcedures);
		}

		@Override
		public SerialForm<? extends Subgroup<?>> getDescriptorBuilder() {
			return descriptorBuilder;
		}

		@JsonProperty("purityMeasurementProcedure")
		public MeasurementProcedure<? extends Measure, ? super Subgroup<?>> purityMeasureProcedure() {
			return purityMeasurementProcedure;
		}

		@JsonProperty("additionalMeasurementProcedures")
		public List<MeasurementProcedure<? extends Measure, ? super Subgroup<?>>> additionalMeasurementProcedures() {
			return additionalMeasurementProcedures;
		}

		@Override
		public PureModelSubgroup build(Workspace context) {
			Subgroup<?> descriptor = descriptorBuilder.build(context);
			return pureSubgroup(descriptor, purityMeasurementProcedure, additionalMeasurementProcedures);
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof PureModelSubgroupBuilderImplementation)) {
				return false;
			}
			PureModelSubgroupBuilderImplementation otherBuilder = (PureModelSubgroupBuilderImplementation) other;
			return (Objects.equals(this.descriptorBuilder, otherBuilder.descriptorBuilder)
					&& Objects.equals(this.purityMeasurementProcedure, otherBuilder.purityMeasurementProcedure)
					&& Objects.equals(this.additionalMeasurementProcedures,
							otherBuilder.additionalMeasurementProcedures));
		}

	}

	private static class PureModelSubgroupImplementation extends DefaultPattern<Subgroup<?>>
			implements PureModelSubgroup {

		private final Function<PureModelSubgroup, PatternBuilder<Subgroup<?>, PureModelSubgroup>> toBuilderConverter;

		private final Measure purityMeasure;

		public PureModelSubgroupImplementation(Population population, Subgroup<?> descriptor,
				List<Measurement> measurements, Measure purityMeasure,
				Function<PureModelSubgroup, PatternBuilder<Subgroup<?>, PureModelSubgroup>> toBuilderConverter) {
			super(population, descriptor, measurements);
			this.purityMeasure = purityMeasure;
			this.toBuilderConverter = toBuilderConverter;
		}

		@Override
		public Subgroup<?> descriptor() {
			return (Subgroup<?>) super.descriptor();
		}

		@Override
		public Measure purityGainMeasure() {
			return purityMeasure;
		}

		@Override
		public SerialForm<PureModelSubgroup> serialForm() {
			return toBuilderConverter.apply(this);
		}

	}

}
