/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;

/**
 * Procedure for computing the interestingness of a sequence pattern in a
 * sequence database. This is defined as the harmonic mean between the
 * frequency and the cohesion of a sequence pattern.
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public enum DefaultInterestingnessMeasurementProcedure implements MeasurementProcedure<Measure,PatternDescriptor> {

	INSTANCE;

	private DefaultInterestingnessMeasurementProcedure() {
		;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return SequenceDescriptor.class.isAssignableFrom(descriptor.getClass());
	}

	@Override
	public Measure getMeasure() {
		return QualityMeasureId.INTERESTINGNESS;
	}

	
	@Override
	public Measurement perform(PatternDescriptor descriptor) {

		double support = Sequences.SEQUENCE_SUPPORT_MEASUREMENT_PROCEDURE.perform(descriptor).value();
		double cohesion = Sequences.COHESION_MEASUREMENT_PROCEDURE.perform(descriptor).value();
		double interestingness = (support / ((SequenceDescriptor)descriptor).sequentialPropositionalLogic().sequences().size()) * cohesion;
		
		return Measures.measurement(QualityMeasureId.INTERESTINGNESS, interestingness);
	}

}
