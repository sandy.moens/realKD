/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.sequences.SequentialPropositionalLogic;
import de.unibonn.realkd.patterns.util.DefaultPropositionList;
import de.unibonn.realkd.patterns.util.PropositionList;
import de.unibonn.realkd.patterns.util.PropositionListBuilder;

/**
 * Implementation of a descriptor for a sequential list of proposition lists.
 * The ordering in the list imposes a predecessor relation either in time
 * or in distance, compared to the following lists. That is the propositions
 * in the first list, are earlier in time / closer in distance than the
 * propositions in the second list, etc. 
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.0
 * 
 * @version 0.5.1
 
 */
public class DefaultSequenceDescriptor implements SequenceDescriptor {
	
	public static SequenceDescriptor create(SequentialPropositionalLogic sequenceDatabase){ 
		return new DefaultSequenceDescriptor(sequenceDatabase, newArrayList());
	}
	
	public static SequenceDescriptor create(SequentialPropositionalLogic sequenceDatabase, List<PropositionList> orderedSetsDescriptors){ 
		return new DefaultSequenceDescriptor(sequenceDatabase, orderedSetsDescriptors);
	}
	
	private SequentialPropositionalLogic sequentialPropositionalLogic;
	private List<PropositionList> orderedSetsDescriptors;

	private DefaultSequenceDescriptor(SequentialPropositionalLogic sequentialPropositionalLogic, List<PropositionList> orderedSetsDescriptors) {
		this.sequentialPropositionalLogic = sequentialPropositionalLogic;
		this.orderedSetsDescriptors = orderedSetsDescriptors;
	}
	
	@Override
	public SequentialPropositionalLogic sequentialPropositionalLogic() {
		return this.sequentialPropositionalLogic;
	}
	

	@Override
	public List<PropositionList> orderedSets() {
		return orderedSetsDescriptors;
	}
	
	public String toString() {
		return String.join(" > ", orderedSetsDescriptors.stream().map(o -> o.toString()).collect(toList()));
	}

	@Override
	public SerialForm<SequenceDescriptor> serialForm() {
		return new DefaultSequenceDescriptorBuilder(this.sequentialPropositionalLogic.identifier(),
				this.orderedSetsDescriptors.stream().map(d -> d.serialForm()).collect(Collectors.toList()));
	}
	
	/**
	 * Creates a new sequence descriptor builder with one empty proposition list.
	 * 
	 * @param propositionalLogicIdentifier
	 * 			the identifier of the propositional logic
	 * 
	 * @return a new sequence descriptor builder
	 */
	public static SerialForm<SequenceDescriptor> defaultSequenceDescriptorBuilder(String propositionalLogicIdentifier) {
		return new DefaultSequenceDescriptorBuilder(propositionalLogicIdentifier,
				newArrayList(DefaultPropositionList.defaultPropositionListBuilder(propositionalLogicIdentifier)));
	}
	
	public static SerialForm<SequenceDescriptor> defaultSequenceDescriptorBuilder(String propositionalLogicIdentifier, 
			List<PropositionListBuilder> orderedSetBuilders) {
		return new DefaultSequenceDescriptorBuilder(propositionalLogicIdentifier,
				orderedSetBuilders);
	}
	
	private static class DefaultSequenceDescriptorBuilder implements SerialForm<SequenceDescriptor> {

		@JsonProperty("propositionalLogicIdentifier")
		private String propositionalLogicIdentifier;
		
		@JsonProperty("orderedSetBuilders")
		private List<PropositionListBuilder> orderedSetBuilders;
		
		@JsonCreator
		public DefaultSequenceDescriptorBuilder(
				@JsonProperty("propositionalLogicIdentifier") String propositionalLogicIdentifier,
				@JsonProperty("orderedSetBuilders") List<PropositionListBuilder> orderedSetBuilders) {
			this.propositionalLogicIdentifier = propositionalLogicIdentifier;
			this.orderedSetBuilders = orderedSetBuilders;
		}

		@Override
		public SequenceDescriptor build(Workspace context) {
			return new DefaultSequenceDescriptor((SequentialPropositionalLogic) context.get(propositionalLogicIdentifier),
					orderedSetBuilders.stream().map(builder -> builder.build(context)).collect(Collectors.toList()));
		}

		public List<PropositionListBuilder> orderedSetsBuilders() {
			return orderedSetBuilders;
		}
		
		@Override
		public Collection<String> dependencyIds() {
			Set<String> dependencies = Sets.newHashSet();
			orderedSetBuilders.stream().forEach(o -> dependencies.addAll(o.dependencyIds()));			
			return Sets.union(ImmutableSet.of(propositionalLogicIdentifier), dependencies);
		}
		
	}
	
}
