/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.SequenceEvent;
import de.unibonn.realkd.data.sequences.SequenceTransaction;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.util.PropositionList;

/**
 * Procedure for computing the cohesion of a sequence pattern in a sequence database.
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public enum DefaultCohesionMeasurementProcedure implements MeasurementProcedure<Measure,PatternDescriptor> {

	INSTANCE;

	private DefaultCohesionMeasurementProcedure() {
		;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return SequenceDescriptor.class.isAssignableFrom(descriptor.getClass());
	}

	@Override
	public Measure getMeasure() {
		return QualityMeasureId.COHESION;
	}

	private int getWindow(SequenceTransaction sequence, List<SequenceEvent> events, List<PropositionList> orderedSets, int startIx) {
		int window = -1;
		int ix = startIx;
		Set<Proposition> propositionsToFind = newHashSet();
		
		for(PropositionList orderedSet: orderedSets) {
			propositionsToFind = newHashSet(orderedSet.propositions());
			while(!propositionsToFind.isEmpty() && ix < events.size()) {
				boolean changed = propositionsToFind.removeAll(events.get(ix).propositions());
				if(window == -1 && changed) {
					window = 0;
				} 
				if (window != -1) {
					window++;
				}
				ix++;
			}
		}			
		if(propositionsToFind.isEmpty() && ix <= events.size()) {
			return window;
		}
		return -1;
	}
	
	private List<PropositionList> getOrderedSetsNoEmpty(SequenceDescriptor descriptor) {
		List<PropositionList> orderedSets = newArrayList(descriptor.orderedSets());
		for(int j = orderedSets.size() - 1; j >= 0; j--) {
			if(orderedSets.get(j).propositions().size() == 0) {
				orderedSets.remove(j);
			}
		}
		return orderedSets;
	}
	
	@Override
	public Measurement perform(PatternDescriptor descriptor) {

		SequenceDescriptor sequenceDescriptor = (SequenceDescriptor) descriptor;

		double totWindow = 0;
		double support = 0;
		for(SequenceTransaction sequence: sequenceDescriptor.sequentialPropositionalLogic().sequences()) {
			int minWindow = -1;
			for(int i = 0; i <  sequence.events().size(); i++) {
				int window = getWindow(sequence, newArrayList(sequence.events()), getOrderedSetsNoEmpty(sequenceDescriptor), i);
				if(window != -1) {
					if(minWindow == -1) {
						minWindow = window;
					} else{
						minWindow = Math.min(minWindow, window);
					}
				}
				if(minWindow == getOrderedSetsNoEmpty(sequenceDescriptor).size()) {
					//Can not get smaller!
					break;
				}
			}
			if(minWindow != -1) {
				support++;
				totWindow += minWindow;
			}
		}
		double avgWindow = totWindow / support;
		double cohesion = 1. * getOrderedSetsNoEmpty(sequenceDescriptor).size() / avgWindow;

		return Measures.measurement(QualityMeasureId.COHESION, cohesion);
	}

}
