/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.emm;

import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.subgroups.Subgroup;

/**
 * <p>
 * Subtype of measurement procedures that are based on distance measures between
 * local and global model of an {@link Subgroup}.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.2.2
 *
 */
public interface ModelDistanceMeasurementProcedure extends MeasurementProcedure<ModelDeviationMeasure,PatternDescriptor> {

	@Override
	public default boolean isApplicable(PatternDescriptor descriptor) {
		if (!(descriptor instanceof Subgroup)) {
			return false;
		}
		return isApplicable(((Subgroup<?>) descriptor).referenceModel(),
				((Subgroup<?>) descriptor).localModel());
	}

	/**
	 * Applicability-test of measurement procedure.
	 * 
	 * @param g
	 *            global model
	 * @param l
	 *            local model
	 * @return whether measurement procedure is applicable to emm pattern
	 *         descriptor with given models
	 */
	public boolean isApplicable(Model g, Model l);

	/**
	 * Actual measurement procedure; can be reduced to measurement on global and
	 * local model.
	 * 
	 * @param descriptor
	 *            must be EMMPatternDescriptor
	 * 
	 */
	@Override
	public default Measurement perform(PatternDescriptor descriptor) {
		return perform(((Subgroup<?>) descriptor).referenceModel(),
				((Subgroup<?>) descriptor).localModel());
	}

	public Measurement perform(Model g, Model l);

}
