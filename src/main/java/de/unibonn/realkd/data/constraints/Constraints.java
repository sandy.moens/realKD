/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.constraints;

import java.text.NumberFormat;
import java.util.Comparator;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

/**
 * Provides static factory methods for the construction of constraints.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public final class Constraints {

	private Constraints() {
		; // not to be instantiated
	}

	/**
	 * 
	 * @param value
	 *            the value for which equality is checked by the constraint
	 * 
	 * @return constraint that holds for all values equal to given value
	 */
	public static <T> Constraint<T> equalTo(T value) {
		return new EqualsConstraint<T>(value);
	}

	public static <T> Constraint<T> greaterThan(T value, Comparator<T> comparator) {
		return new GreaterThanConstraint<T>(value, comparator);
	}

	public static <T> Constraint<T> lessThan(T value, Comparator<T> comparator) {
		return new LessThanConstraint<T>(value, comparator);
	}

	public static Constraint<Double> greaterThan(double value) {
		return Constraints.greaterThan(value, Comparator.naturalOrder());
	}

	public static Constraint<Double> lessThan(double value) {
		return Constraints.lessThan(value, Comparator.naturalOrder());
	}

	public static Constraint<Double> lessOrEquals(double threshold) {
		Constraint<Double> equalsConstraint = equalTo(threshold);
		Constraint<Double> lessThanConstraint = lessThan(threshold);
		return or(lessThanConstraint, equalsConstraint);
	}

	public static Constraint<Double> greaterOrEquals(double threshold) {
		Constraint<Double> equalsConstraint = equalTo(threshold);
		Constraint<Double> greaterThanConstraint = greaterThan(threshold);
		return or(greaterThanConstraint, equalsConstraint);
	}

	public static <T> Constraint<T> greaterOrEquals(T threshold, Comparator<T> order) {
		Constraint<T> equalsConstraint = equalTo(threshold);
		Constraint<T> greaterThanConstraint = greaterThan(threshold, order);
		return or(greaterThanConstraint, equalsConstraint);
	}

	public static <T> Constraint<T> lessOrEquals(T threshold, Comparator<T> order) {
		Constraint<T> equalsConstraint = equalTo(threshold);
		Constraint<T> lessThanConstraint = lessThan(threshold, order);
		return or(lessThanConstraint, equalsConstraint);
	}

	public static Constraint<Double> inClosedInterval(double lowerThreshold, double upperThreshold) {
		Constraint<Double> lowerBorder = greaterOrEquals(lowerThreshold);
		Constraint<Double> upperBorder = lessOrEquals(upperThreshold);
		return and(lowerBorder, upperBorder);
	}

	public static <T> Constraint<T> and(Constraint<T> firstEntailedConstraint, Constraint<T> secondEntailedConstraint) {
		return new AndConstraint<T>(firstEntailedConstraint, secondEntailedConstraint);
	}

	public static <T> Constraint<T> or(Constraint<T> firstEntailedConstraint, Constraint<T> secondEntailedConstraint) {
		return new OrConstraint<T>(firstEntailedConstraint, secondEntailedConstraint);
	}

	/**
	 * Returns constraint equivalent to another constraint, adding specific
	 * suffix-notation-name and description.
	 * 
	 * @author Mario Boley
	 * 
	 * @since 0.1.0
	 * 
	 * @version 0.3.0
	 * 
	 */
	public static <T> Constraint<T> namedConstraint(Constraint<T> entailedConstraint, String suffixName,
			String description) {
		return new NamedConstraint<T>(entailedConstraint, suffixName, description);
	}

	private static class NamedConstraint<T> implements DerivedConstraint<T> {

		@JsonProperty("entailedConstraint")
		private final Constraint<T> entailedConstraint;
		@JsonProperty("name")
		private final String name;
		@JsonProperty("description")
		private final String description;

		@JsonCreator
		private NamedConstraint(@JsonProperty("entailedConstraint") Constraint<T> entailedConstraint,
				@JsonProperty("name") String name, @JsonProperty("description") String description) {
			this.entailedConstraint = entailedConstraint;
			this.name = name;
			this.description = description;
		}

		@Override
		public boolean holds(T value) {
			return entailedConstraint.holds(value);
		}

		@Override
		public String description() {
			return description;
		}

		@Override
		public String suffixNotationName() {
			return name;
		}

		@Override
		public boolean implies(Constraint<T> anotherConstraint) {
			return entailedConstraint.implies(anotherConstraint);
		}

		@Override
		public boolean impliedBy(Constraint<T> anotherConstraint) {
			return anotherConstraint.implies(entailedConstraint);
		}

	}

	public static Constraint<Integer> divisibleBy(int divisor) {
		return new DivisibleConstraint(divisor);
	}

	public static Constraint<Integer> notDivisibleBy(int divisor) {
		return new NotDivisibleConstraint(divisor);
	}

	/**
	 * @param ordinal
	 * @param threshold
	 * @return
	 */
	public static <T> Constraint<T> lowerQuantileBoundNegation(OrdinalAttribute<T> ordinal, T threshold) {
		final String name = topCutName(
				(double) ordinal.inverseOrderNumber(threshold) / ordinal.numberOfNonMissingValues());
		return namedConstraint(lessThan(threshold, ordinal.valueComparator()), " not in " + name,
				"not in " + name + " [-inf, " + threshold + ")");
	}

	/**
	 * @param ordinal
	 * @param threshold
	 * @return
	 */
	public static <T> Constraint<T> lowerQuantileBound(OrdinalAttribute<T> ordinal, T threshold) {
		final String name = topCutName(
				(double) ordinal.inverseOrderNumber(threshold) / ordinal.numberOfNonMissingValues());
		Constraint<T> topConstraint = namedConstraint(greaterOrEquals(threshold, ordinal.valueComparator()),
				" in " + name, "in " + name + " [" + threshold + ", inf]");
		return topConstraint;
	}

	/**
	 * @param ordinal
	 * @param threshold
	 * @return
	 */
	public static <T> Constraint<T> upperQuantileBound(OrdinalAttribute<T> ordinal, T threshold) {
		final String name = bottomCutName((double) ordinal.orderNumber(threshold) / ordinal.numberOfNonMissingValues());
		return namedConstraint(lessOrEquals(threshold, ordinal.valueComparator()), " in " + name,
				"in " + name + " [-inf, " + threshold + "]");
	}

	/**
	 * @param ordinal
	 * @param threshold
	 * @return
	 */
	public static <T> Constraint<T> upperQuantileBoundNegation(OrdinalAttribute<T> ordinal, T threshold) {
		final String name = bottomCutName((double) ordinal.orderNumber(threshold) / ordinal.numberOfNonMissingValues());
		return namedConstraint(greaterThan(threshold, ordinal.valueComparator()), " not in " + name,
				"not in " + name + " (" + threshold + ", inf]");
	}

	private final static NumberFormat QUANTILE_BASED_CONSTRAINT_NUMBER_FORMAT = NumberFormat.getPercentInstance();
	{
		QUANTILE_BASED_CONSTRAINT_NUMBER_FORMAT.setMaximumFractionDigits(1);
	}

	private static String topCutName(double cutPoint) {
		return "top " + QUANTILE_BASED_CONSTRAINT_NUMBER_FORMAT.format(cutPoint);
	}

	private static String bottomCutName(double cutPoint) {
		return "bottom " + QUANTILE_BASED_CONSTRAINT_NUMBER_FORMAT.format(cutPoint);
	}

}
