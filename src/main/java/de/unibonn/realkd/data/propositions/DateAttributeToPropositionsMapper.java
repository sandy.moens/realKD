/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.propositions;

import static com.google.common.collect.Sets.newTreeSet;

import java.util.Date;
import java.util.List;

import de.unibonn.realkd.data.constraints.Constraints;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultDateAttribute;

/**
 * @author Sandy
 *
 */
public enum DateAttributeToPropositionsMapper implements AttributeToPropositionsMapper {

	YEAR_MONTH_DATE_HOUR {
		@Override
		public <T> void constructPropositions(Attribute<T> attribute, List<AttributeBasedProposition<?>> result) {
			if(attribute instanceof DefaultDateAttribute) {
				for (Date date : newTreeSet(((DefaultDateAttribute)attribute).nonMissingValues())) {
					result.add(new DefaultAttributeBasedProposition<>((Attribute<?>) attribute,
							Constraints.equalTo(date),result.size()));
//							new EqualsConstraint<>(date){
//								public String getSuffixNotationName() { return "="+new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").format(date);}
//							}, 
				}
			}
		}
	};

	@Override
	public abstract <T> void constructPropositions(Attribute<T> attribute, List<AttributeBasedProposition<?>> result);
}
