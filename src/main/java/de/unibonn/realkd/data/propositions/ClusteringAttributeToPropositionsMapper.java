/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.propositions;

import static de.unibonn.realkd.util.Lists.kMeansCutPoints;

import java.text.NumberFormat;
import java.util.List;

import de.unibonn.realkd.data.constraints.Constraint;
import de.unibonn.realkd.data.constraints.Constraints;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 * <p>
 * Mappers that create propositions from metric attributes corresponding to an
 * <it>even</it> number of cut-off points on the empirical range of that
 * attribute. The cut-off points can be irregular (non-equal width). In the
 * current implementation (version 0.3.2) cut-off points are found via k-means
 * clustering with a limited number of iterations.
 * </p>
 * <p>
 * All of these mappers are elements of
 * {@link PropositionalLogicFromTableBuilder#ALL_MAPPERS} and are thus valid
 * options for propositional logic construction via that builder.
 * <p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.2
 * 
 * @version 0.3.2
 * 
 * @see ClusteringAttributeToNamedPropositionsMapper
 *
 */
public enum ClusteringAttributeToPropositionsMapper implements AttributeToPropositionsMapper {

	CLUSTERING_2_CUTOFFS(2),

	CLUSTERING_4_CUTOFFS(4),

	CLUSTERING_6_CUTOFFS(6),

	CLUSTERING_8_CUTOFFS(8),

	CLUSTERING_10_CUTOFFS(10),

	CLUSTERING_12_CUTOFFS(12),

	CLUSTERING_14_CUTOFFS(14),

	CLUSTERING_16_CUTOFFS(16),

	CLUSTERING_18_CUTOFFS(18),

	CLUSTERING_20_CUTOFFS(20);

	/**
	 * 
	 */
	private static final NumberFormat FORMAT = NumberFormat.getInstance();

	private static final int KMEANS_ITERATIONS = 25;

	private final int numberOfCutOffs;

	private ClusteringAttributeToPropositionsMapper(int numberOfCutOffs) {
		this.numberOfCutOffs = numberOfCutOffs;
	}

	@Override
	public <T> void constructPropositions(Attribute<T> attribute, List<AttributeBasedProposition<?>> result) {
		if (attribute instanceof MetricAttribute) {
			MetricAttribute metricAttribute = (MetricAttribute) attribute;
			List<Double> cutPoints = kMeansCutPoints(metricAttribute.nonMissingValuesInOrder(), numberOfCutOffs + 1,
					KMEANS_ITERATIONS);
			for (int i = 0; i < numberOfCutOffs / 2; i++) {
				result.add(lessThanConstraint(metricAttribute, cutPoints.get(i), result.size()));
				result.add(notLessThanConstraint(metricAttribute, cutPoints.get(i), result.size()));
			}
			for (int i = numberOfCutOffs / 2; i < cutPoints.size(); i++) {
				result.add(greaterThanConstraint(metricAttribute, cutPoints.get(i), result.size()));
				result.add(notGreaterThanConstraint(metricAttribute, cutPoints.get(i), result.size()));
			}
		}
	}

	private DefaultAttributeBasedProposition<Double> lessThanConstraint(MetricAttribute metricAttribute, Double value,
			int id) {
		Constraint<Double> lessThan = Constraints.lessThan(value);
		String description = "in [-inf," + FORMAT.format(value) + ")";
		return new DefaultAttributeBasedProposition<Double>(metricAttribute,
				Constraints.namedConstraint(lessThan, "<" + FORMAT.format(value), description), id);
	}

	private DefaultAttributeBasedProposition<Double> notLessThanConstraint(MetricAttribute metricAttribute,
			Double value, int id) {
		Constraint<Double> largerThan = Constraints.greaterOrEquals(value);
		String description = "in [" + FORMAT.format(value) + ", inf]";
		return new DefaultAttributeBasedProposition<Double>(metricAttribute,
				Constraints.namedConstraint(largerThan, ">=" + FORMAT.format(value), description), id);
	}

	private DefaultAttributeBasedProposition<Double> greaterThanConstraint(MetricAttribute metricAttribute,
			Double value, int id) {
		Constraint<Double> greaterThan = Constraints.greaterThan(value);
		String description = "in (" + FORMAT.format(value) + ",inf]";
		return new DefaultAttributeBasedProposition<Double>(metricAttribute,
				Constraints.namedConstraint(greaterThan, ">" + FORMAT.format(value), description), id);
	}

	private DefaultAttributeBasedProposition<Double> notGreaterThanConstraint(MetricAttribute metricAttribute,
			Double value, int id) {
		Constraint<Double> lessThan = Constraints.lessOrEquals(value);
		String description = "in [-inf," + FORMAT.format(value) + "]";
		return new DefaultAttributeBasedProposition<Double>(metricAttribute,
				Constraints.namedConstraint(lessThan, "=<" + FORMAT.format(value), description), id);
	}

}
