/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.branchbound;

import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.logging.Logger;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.ComputationMeasure;
import de.unibonn.realkd.patterns.Pattern;

/**
 * <p>
 * Implements best-first branch-and-bound search for maximizing some target
 * function with a given optimistic estimator. Search starts in a provided root
 * search node and then successively refines nodes by a provided refinement
 * operator.
 * </p>
 * 
 * @param R
 *            the result type
 * 
 * @param N
 *            the search node type
 * 
 * @author Mario Boley
 * 
 * @author Panagiotis Mandros
 * 
 * @since 0.4.0
 * 
 * @version 0.4.1
 *
 */
public class OPUS<R extends Pattern<?>, N> extends AbstractMiningAlgorithm {

	private static final Logger LOGGER = Logger.getLogger(BestFirstBranchAndBound.class.getName());

	private static class EvaluatedNode<N> {

		public final N content;

		public final double potential;

		public final double value;

		public final int depth;

		public final Optional<Function<? super N, ? extends N>> opOperatorUsed;

		public EvaluatedNode(N content, Optional<Function<? super N, ? extends N>> opOperatorUsed, double potential,
				double value, int depth) {
			this.content = content;
			this.opOperatorUsed = opOperatorUsed;
			this.potential = potential;
			this.value = value;
			this.depth = depth;
		}

		// @Override
		// public int compareTo(EvaluatedNode<?> o) {
		// /*
		// * smaller in preference order if this potential larger than other
		// * potential, a.k.a priority element in the queue will be the one
		// * with the best potential. This is used for the boundary queue. For
		// * the best result queue its different
		// */
		// return Double.compare(o.potential, this.potential);
		// }

	}

	private static class BoundaryNode<N> extends EvaluatedNode<N> implements Comparable<BoundaryNode<?>> {

		public final Set<Function<? super N, ? extends N>> active;

		public BoundaryNode(N content, Set<Function<? super N, ? extends N>> active,
				Optional<Function<? super N, ? extends N>> opOperatorUsed, double potential, double value, int depth) {
			super(content, opOperatorUsed, potential, value, depth);
			this.active = active;
		}

		@Override
		public int compareTo(BoundaryNode<?> o) {
			/*
			 * smaller in preference order if this potential larger than other
			 * potential, a.k.a priority element in the queue will be the one
			 * with the best potential. This is used for the boundary queue. For
			 * the best result queue its different
			 */
			return Double.compare(o.potential, this.potential);
		}

	}

	private static class UnevaluatedNode<N> {
		public final Function<? super N, ? extends N> opOperatorUsed;
		public final N content;

		public UnevaluatedNode(N content, Function<? super N, ? extends N> opOperatorUsed) {
			this.opOperatorUsed = opOperatorUsed;
			this.content = content;
		}
	}

	private static <N> EvaluatedNode<N> evaluatedNode(N content,
			Optional<Function<? super N, ? extends N>> operatorUsed, double potential, double value, int depth) {
		return new EvaluatedNode<N>(content, operatorUsed, potential, value, depth);
	}

	private static <N> BoundaryNode<N> boundaryNode(EvaluatedNode<N> evalNode,
			Set<Function<? super N, ? extends N>> active) {
		return new BoundaryNode<N>(evalNode.content, new HashSet<>(active), evalNode.opOperatorUsed, evalNode.potential,
				evalNode.value, evalNode.depth);
	}

	private static <N> UnevaluatedNode<N> unevaluatedNode(N content, Function<? super N, ? extends N> opOperatorUsed) {
		return new UnevaluatedNode<N>(content, opOperatorUsed);
	}

	private final OperatorOrder distributionOrder;

	private final ToDoubleFunction<? super N> f;

	private final ToDoubleFunction<? super N> oest;

	private final Predicate<N>[] additionalPruningRules;

	private final Function<? super N, ? extends R> toPattern;

	private final PriorityQueue<BoundaryNode<N>> boundary;

	// alpha approximation of the best possible solution
	private final double alpha;

	// the size of the result queue
	private final int numberOfResults;

	private final Optional<Integer> depthLimit;

	private PriorityQueue<EvaluatedNode<N>> best;

	private int nodesCreated = 1;

	private int nodesDiscardedPotential = 0;

	private int nodesDiscardedPruning = 0;

	private int maxDepth = 0;

	private int maxBoundarySize = 1;

	// depth of the first encounter of the best solution
	private int solutionDepth = 0;

	// stores the best solution so far, to keep track of depth
	private EvaluatedNode<N> bestSolution;

	@SafeVarargs
	public OPUS(Function<? super N, ? extends R> toPattern, Set<Function<? super N, ? extends N>> active, N root,
			ToDoubleFunction<? super N> f, ToDoubleFunction<? super N> oest, int numberOfResults, double alpha,
			Optional<Integer> depthLimit, OperatorOrder distributionOrder, Predicate<N>... additionalPruningRules) {
		this.f = f;
		this.oest = oest;
		this.distributionOrder = distributionOrder;
		this.boundary = new PriorityQueue<>();
		this.toPattern = toPattern;
		this.numberOfResults = numberOfResults;
		this.alpha = alpha;
		this.depthLimit = depthLimit;
		this.additionalPruningRules = additionalPruningRules;

		// initialize search
		EvaluatedNode<N> evaluatedRoot = evaluatedNode(root, Optional.empty(), oest.applyAsDouble(root),
				f.applyAsDouble(root), 0);
		/*
		 * priority element in the queue will be the one with the smallest value
		 */
		best = new PriorityQueue<>((n, m) -> Double.compare(n.value, m.value));
		best.add(evaluatedRoot);
		bestSolution = evaluatedRoot;
		boundary.add(boundaryNode(evaluatedRoot, active));

	}

	public OPUS(Function<? super N, ? extends R> toPattern, Set<Function<? super N, ? extends N>> active, N root,
			ToDoubleFunction<? super N> f, ToDoubleFunction<? super N> oest) {
		this(toPattern, active, root, f, oest, 1, 1, Optional.empty(), OperatorOrder.OPUS_PAPER);
	}

	private void updateResults(EvaluatedNode<N> candidate) {
		/*
		 * if the queue has space insert candidate
		 */
		if (best.size() < numberOfResults) {
			best.add(candidate);
			// check if tracked metrics have to be updated (non crucial for
			// result)
			if (candidate.value > bestSolution.value) {
				bestSolution = candidate;
				solutionDepth = candidate.depth;
				LOGGER.info("Best solution updated: " + bestSolution.content);
			}
		}
		/*
		 * if the queue has no space, and the candidate has bigger value than
		 * the least best current solution, poll the queue, and insert the
		 * candidate
		 */
		else if ((candidate.value > best.peek().value) && (best.size() == numberOfResults)) {
			best.poll();
			best.add(candidate);
			if (candidate.value > bestSolution.value) {
				bestSolution = candidate;
				solutionDepth = candidate.depth;
				LOGGER.info("Best solution updated: " + bestSolution.content);

			}
		}
	}

	private EvaluatedNode<N> evaluateNode(N n, Function<? super N, ? extends N> operatorUsed, int depth) {
		maxDepth = Math.max(maxDepth, depth);
		return evaluatedNode(n, Optional.of(operatorUsed), oest.applyAsDouble(n), f.applyAsDouble(n), depth);
	}

	private boolean hasPotential(EvaluatedNode<N> candidate) {
		if (candidate.potential > best.peek().value / alpha) {
			return true;
		} else {
			return false;
		}
	}

	// @Override
	// protected Collection<Pattern<?>> concreteCall() {
	// while (!stopRequested() && !boundary.isEmpty() &&
	// hasPotential(boundary.peek())) {
	// // list here and never change once it is passed to node
	// BoundaryNode<N> topPotentialNode = boundary.poll();
	// Set<Function<? super N, ? extends N>> remainingOperators = new
	// HashSet<>(topPotentialNode.active);
	// // refines according to activeOperators
	// // TODO can we entangle node creation (type N) and evaluated node
	// // creation in order to apply additional pruning rules already
	// // before evaluation
	// List<EvaluatedNode<N>> newNodes = remainingOperators.stream()
	// .map(n -> evaluateNode(n.apply(topPotentialNode.content), n,
	// topPotentialNode.depth + 1))
	// .collect(toList());
	// nodesCreated += newNodes.size();
	//
	// filterBasedOnAdditionalPruningRules(remainingOperators, newNodes);
	//
	// // updateResults
	// newNodes.forEach(this::updateResults);
	// // log
	// if ((nodesCreated - remainingOperators.size()) / 10000 < nodesCreated /
	// 10000) {
	// logStats();
	// }
	// // prune and update boundary
	// if (!depthLimit.isPresent() || depthLimit.get() - 1 >
	// topPotentialNode.depth) {
	// filterBasedOnPotential(remainingOperators, newNodes);
	// // assign active operators to children non-redundantly
	// distributionOrder.apply(newNodes);
	// for (EvaluatedNode<N> tempNode : newNodes) {
	// remainingOperators.remove(tempNode.opOperatorUsed.get());
	// boundary.add(boundaryNode(tempNode, remainingOperators));
	// }
	// trackBoundarySize();
	// }
	// }
	//
	// logStats();
	//
	// List<EvaluatedNode<N>> resultNodes = new ArrayList<>(best);
	// sort(resultNodes, (n, m) -> Double.compare(m.value, n.value));
	// List<Pattern<?>> result = resultNodes.stream().map(n ->
	// toPattern.apply(n.content)).collect(toList());
	// return result;
	// }

	@Override
	protected Collection<Pattern<?>> concreteCall() {
		while (!stopRequested() && !boundary.isEmpty() && hasPotential(boundary.peek())) {
			// list here and never change once it is passed to node
			BoundaryNode<N> topPotentialNode = boundary.poll();
			Set<Function<? super N, ? extends N>> remainingOperators = new HashSet<>(topPotentialNode.active);

			// create the unevaluated nodes
			List<UnevaluatedNode<N>> unevaluatedNodes = remainingOperators.stream()
					.map(n -> unevaluatedNode(n.apply(topPotentialNode.content), n)).collect(toList());
			nodesCreated += unevaluatedNodes.size();
			// prune based on rules
			filterBasedOnAdditionalPruningRules(remainingOperators, unevaluatedNodes);

			// evaluate nodes
			List<EvaluatedNode<N>> newNodes = unevaluatedNodes.stream()
					.map(n -> evaluateNode(n.content, n.opOperatorUsed, topPotentialNode.depth + 1)).collect(toList());

			// updateResults
			newNodes.forEach(this::updateResults);
			// log
			if ((nodesCreated - remainingOperators.size()) / 10000 < nodesCreated / 10000) {
				logStats();
			}
			// prune and update boundary
			if (!depthLimit.isPresent() || depthLimit.get() - 1 > topPotentialNode.depth) {
				filterBasedOnPotential(remainingOperators, newNodes);
				// assign active operators to children non-redundantly
				distributionOrder.apply(newNodes);
				for (EvaluatedNode<N> tempNode : newNodes) {
					remainingOperators.remove(tempNode.opOperatorUsed.get());
					boundary.add(boundaryNode(tempNode, remainingOperators));
				}
				trackBoundarySize();
			}
		}

		logStats();

		List<EvaluatedNode<N>> resultNodes = new ArrayList<>(best);
		sort(resultNodes, (n, m) -> Double.compare(m.value, n.value));
		List<Pattern<?>> result = resultNodes.stream().map(n -> toPattern.apply(n.content)).collect(toList());
		return result;
	}

	private void filterBasedOnAdditionalPruningRules(Set<Function<? super N, ? extends N>> remainingOperators,
			List<UnevaluatedNode<N>> unevaluatedNodes) {
		for (int i = unevaluatedNodes.size() - 1; i >= 0; i--) {
			UnevaluatedNode<N> node = unevaluatedNodes.get(i);
			for (Predicate<N> rule : additionalPruningRules) {
				if (rule.test(node.content)) {
					remainingOperators.remove(node.opOperatorUsed);
					unevaluatedNodes.remove(i);
					nodesDiscardedPruning++;
					// only one rule has to apply
					break;
				}
			}
		}
	}

	private void filterBasedOnPotential(Set<Function<? super N, ? extends N>> remainingOperators,
			List<EvaluatedNode<N>> newNodes) {
		for (int i = newNodes.size() - 1; i >= 0; i--) {
			EvaluatedNode<N> node = newNodes.get(i);
			if (!hasPotential(node)) {
				remainingOperators.remove(node.opOperatorUsed.get());
				newNodes.remove(i);
				nodesDiscardedPotential++;
			}
		}
	}

	public static enum OperatorOrder {

		OPUS_PAPER {
			public void apply(List<? extends EvaluatedNode<?>> newNodes) {
				sort(newNodes, (n, m) -> Double.compare(n.potential, m.potential));
			}
		},

		/**
		 * Sticks with given order.
		 */
		TRIVIAL {
			public void apply(List<? extends EvaluatedNode<?>> newNodes) {
				;
			}
		};

		public abstract void apply(List<? extends EvaluatedNode<?>> newNodes);

	}

	private void trackBoundarySize() {
		if (boundary.size() > maxBoundarySize) {
			maxBoundarySize = boundary.size();
		}
	}

	private void logStats() {
		// LOGGER.info(nodesDiscarded + "/" + nodesCreated + "(" + (1.0 *
		// nodesDiscarded / nodesCreated) + ")"
		// + " nodes discarded/created (" + solutionDepth + "/" + maxDepth + "
		// best solution depth/max depth)");

		double kBest = best.peek().value;

		double howCloseToBestPossibleSolution;
		if (!boundary.isEmpty()) {
			howCloseToBestPossibleSolution = kBest / boundary.peek().potential;
		} else {
			howCloseToBestPossibleSolution = 1;
		}
		// LOGGER.info(nodesDiscardedPotential + "/" + nodesCreated + "(" + (1.0
		// * nodesDiscardedPotential / nodesCreated)
		// + ")" + " nodes discarded/created (" + solutionDepth + "/" + maxDepth
		// + " best solution depth/max depth)" + "\n" + "kth-best found -
		// potential left: " + best.peek().value
		// + " - "
		// + (boundary.isEmpty() ? "Nil"
		// : String.valueOf(boundary.peek().potential) + " (" +
		// howCloseToBestPossibleSolution + ") " + " "
		// + "\n" + boundary.peek().content)
		// + "\n" + "Size of boundary queue: " + boundary.size());

		LOGGER.info("Nodes created: " + nodesCreated + "\n" + "Nodes discarded due to potential: "
				+ nodesDiscardedPotential + "\n" + "Nodes discarded due to pruning rules: " + nodesDiscardedPruning
				+ "\n" + "best solution depth/max depth: " + solutionDepth + "/" + maxDepth + "\n"
				+ "kth-best found - potential left: " + best.peek().value + " - "
				+ (boundary.isEmpty() ? "Nil"
						: String.valueOf(boundary.peek().potential) + " (" + howCloseToBestPossibleSolution + ") " + " "
								+ "\n" + boundary.peek().content)
				+ "\n" + "Size of boundary queue: " + boundary.size());

		// LOGGER.info("kth-best found - potential left: " + best.peek().value +
		// " - "
		// + (boundary.isEmpty() ? "Nil"
		// : String.valueOf(boundary.peek().potential) + " (" +
		// howCloseToBestPossibleSolution + ") " + " " +
		// boundary.peek().content));
		// LOGGER.info("Size of boundary queue: " + boundary.size());
	}

	@Override
	public String caption() {
		return "";
	}

	@Override
	public String description() {
		return "";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.OTHER;
	}

	@Override
	public Optional<Double> value(ComputationMeasure measure) {
		if (measure == BranchAndBoundMeasures.NODES_CREATED) {
			return Optional.of((double) nodesCreated);
		} else if (measure == BranchAndBoundMeasures.NODES_DISCARDED_POTENTIAL) {
			return Optional.of((double) nodesDiscardedPotential);
		} else if (measure == BranchAndBoundMeasures.NODES_DISCARDED_PRUNING_RULES) {
			return Optional.of((double) nodesDiscardedPruning);
		} else if (measure == BranchAndBoundMeasures.MAX_DEPTH) {
			return Optional.of((double) maxDepth);
		} else if (measure == BranchAndBoundMeasures.SOLUTION_DEPTH) {
			return Optional.of((double) solutionDepth);
		} else if (measure == BranchAndBoundMeasures.MAX_SIZE_BOUNDARY_QUEUE) {
			return Optional.of((double) maxBoundarySize);
		} else {
			return super.value(measure);
		}
	}

}
