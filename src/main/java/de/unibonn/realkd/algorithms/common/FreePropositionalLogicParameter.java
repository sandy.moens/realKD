package de.unibonn.realkd.algorithms.common;

import java.util.List;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * Propositional logic parameter that accepts any propositional logic in some
 * given data workspace.
 * 
 * @see MatchingPropositionalLogicParameter
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 * 
 */
public class FreePropositionalLogicParameter extends DefaultRangeEnumerableParameter<PropositionalLogic> {

	private static final String DESCRIPTION = "The collections of basic statements avaible to the algorithm to construct patterns.";

	private static final String NAME = "Propositions";

	public FreePropositionalLogicParameter(final Workspace dataWorkspace) {
		super(NAME, DESCRIPTION, PropositionalLogic.class, new RangeComputer<PropositionalLogic>() {
			@Override
			public List<PropositionalLogic> get() {
				return dataWorkspace.getAllPropositionalLogics();
			}
		});
	}

}
